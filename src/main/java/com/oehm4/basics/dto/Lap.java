package com.oehm4.basics.dto;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "Lap_details")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Lap {
	
	@Id
	@GenericGenerator(name="lap_auto", strategy="increment")
	@GeneratedValue(generator="lap_auto")
	private Long id;
	@Column (name="name")
	private String name;
	@Column (name="model")
	private String model;
	@Column (name="price")
	private Long price;
	
	
	public Lap() {
		
		// TODO Auto-generated constructor stub
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public Long getPrice() {
		return price;
	}


	public void setPrice(Long price) {
		this.price = price;
	}
	

	@Override
	public String toString() {
		return "Lap [id=" + id + ", name=" + name + ", model=" + model + ", price=" + price + "]";
	}

}
