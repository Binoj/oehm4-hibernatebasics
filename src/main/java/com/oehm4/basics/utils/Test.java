package com.oehm4.basics.utils;

import com.oehm4.basics.dto.Lap;

import java.util.List;

import com.oehm4.basics.dao.AssociationDAO;
import com.oehm4.basics.dao.HQLLapDAO;

public class Test {
	
	public static void main(String[] args) {
	
	AssociationDAO lapDAO = new AssociationDAO();
	Lap lapdto = new Lap();
	
	

	  lapdto.setModel("B50144"); lapdto.setName("Dell"); lapdto.setPrice(50000L);
	 
	 lapDAO.saveLapDetails(lapdto);
	 
	 
	 lapdto = lapDAO.getLapDetails(1L);
		
		System.out.println("lapDTO" + lapdto);
		lapdto = lapDAO.getLapDetails(1L);
		
		System.out.println("lapDTO" + lapdto);
		
		
		lapDAO.updateLapDetails(1L, "Acer");
		
		lapDAO.deleteLapDetails(1L);
		
		HQLLapDAO hqllapDAO=new HQLLapDAO();
		lapdto.setModel("B50145");
		lapdto.setName("HP");
		lapdto.setPrice(60000L);
		
		 lapDAO.saveLapDetails(lapdto);
		 
		 List<Lap> lapDetails =
				 hqllapDAO.getLapDetailsByLapId(1L);
				  lapDetails.forEach(obj -> { System.out.println(obj);
				  
				  });
				  
				  hqllapDAO.updateLapbyId(1L,"Dell");
				  
				  hqllapDAO.deletebyID(1L);
	}
	

}
