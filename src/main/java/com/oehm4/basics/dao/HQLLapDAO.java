package com.oehm4.basics.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


import com.oehm4.basics.dto.Lap;
import com.oehm4.basics.utils.SessionFactoryUtil;

public class HQLLapDAO {
	
	public void saveLapDetails( Lap lap) {
		Session session = SessionFactoryUtil.createSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(lap);
		transaction.commit();	
	}
	
	public List<Lap> getLapDetailsByLapId(Long ID) {
		Session session = SessionFactoryUtil.createSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		String hql = "from Lap where Id=:ID";
		Query query = session.createQuery(hql);
		query.setParameter("ID", ID);// setting the value for above query
		List list = query.list();
		return list;

	}
	
	public void updateLapbyId(Long ID, String name) {
		Session session = SessionFactoryUtil.createSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		String hql = "update Lap SET name=:name where id=:id";

		Query query = session.createQuery(hql);
		query.setParameter("id", ID);// here it is giving int some confusion
		query.setParameter("name", name);
		int update = query.executeUpdate();
		if (update != 0) {
			System.out.println("update finished");
		} else {
			System.out.println("update failed");
		}
		transaction.commit();

	}

	public void deletebyID(Long id) {
		Session session = SessionFactoryUtil.createSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();String hql = "delete  Lap where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int delete = query.executeUpdate();
		transaction.commit();
		if (delete > 0) {
			System.out.println("delete operation success");
		} else {
			System.out.println("delete operation failed");
		}
	}
}
