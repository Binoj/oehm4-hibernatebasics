package com.oehm4.basics.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


import com.oehm4.basics.dto.Lap;
import com.oehm4.basics.utils.SessionFactoryUtil;

public class AssociationDAO {
	
	public void saveLapDetails( Lap lap) {
		Session session = SessionFactoryUtil.createSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(lap);
		transaction.commit();	
	}
	
	public Lap getLapDetails(Long ID) {
		/*Configuration configuration = new Configuration();
		configuration.configure();
		// configuration.addAnnotatedClass(AccountDTO.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();*/

		
		Session session = SessionFactoryUtil.createSessionFactory().openSession();
		return session.get(Lap.class, ID);

		/*AccountDTO accountDTO = session.get(AccountDTO.class, ID);
		session.getTransaction().commit();
		return accountDTO;*/

	}
	
	public void updateLapDetails(Long ID, String name) {
		Lap lapDTO = getLapDetails(ID);
		if (lapDTO != null) {
			Session session = SessionFactoryUtil.createSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			lapDTO.setName(name);
			session.update(lapDTO);
			transaction.commit();
		} else {
			System.out.println("update of name failed");
		}

		// return accountDTO;
	}
	
	public void deleteLapDetails(Long ID) {
		Lap lapDTO = getLapDetails(ID);
		if (lapDTO != null) {
			Session session = SessionFactoryUtil.createSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			lapDTO.setId(ID);
			session.delete(lapDTO);
			transaction.commit();
		}
	}
	

}
